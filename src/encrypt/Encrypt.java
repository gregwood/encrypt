
package encrypt;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

class Encrypt {

    int checksum;
    private LinkedList<Character> keyList;
    private LinkedList<Character> keyListActual;
        
    public Encrypt() {
        
    }
    
    public Encrypt(String passphrase) {
        checksum = generateChecksum(passphrase);
        initKeyList();
    }
    
    public String en(String thisString, String passphrase) {
        return encrypt(thisString, passphrase);
    }
    
    private String encrypt(String thisString, String passphrase) {
        
        Encrypt en = new Encrypt(passphrase);
                
        StringBuilder encrypted = new StringBuilder();
        encrypted.setLength(thisString.length());
        for (int i = 0; i < encrypted.length(); i++) {
            encrypted.setCharAt(i, en.encryptChar(thisString.charAt(i)));
        }
        
        return encrypted.toString();
    }
    
    public String de(String thisString, String passphrase) {
        return decrypt(thisString, passphrase);
    }
    
    private String decrypt(String thisString, String passphrase) {
        
        Encrypt en = new Encrypt(passphrase);
                   
        StringBuilder decrypted = new StringBuilder();
        decrypted.setLength(thisString.length());
        
        for (int i = 0; i < decrypted.length(); i++) {
            decrypted.setCharAt(i, en.decryptChar(thisString.charAt(i)));
        }
        return decrypted.toString();
    }
    
    //encrypt one file into another
    private void encrypt(File thisFile, File thatFile) throws FileNotFoundException, IOException {
        
        Encrypt en = new Encrypt();
        en.initKeyList();
        
        FileReader fr = new FileReader(thisFile);
        BufferedReader br = new BufferedReader(fr);
        
        FileWriter fw = new FileWriter(thatFile);
        BufferedWriter bw = new BufferedWriter(fw);
            
        String s;
        StringBuilder encrypted = new StringBuilder();
        while ( (s = br.readLine()) != null) {
            encrypted.setLength(s.length());
            for (int i = 0; i < encrypted.length(); i++) {
                encrypted.setCharAt(i, en.encryptChar(s.charAt(i)));
            }
            
            //write to encrypted file.
            bw.write(encrypted.toString());
            bw.newLine();
            
        }
        bw.close();
    }
    
    private int generateChecksum(String passphrase) {
        int checksum = 0;
        for (int i = 0; i < passphrase.length(); i++) {
            int thisChar = passphrase.charAt(i);
            thisChar *= (i + 1) * 1729;
            checksum += thisChar;
        }
        return checksum;
    }
    
    //decrypt one file into another
    private void decrypt(File thisFile, File thatFile) throws FileNotFoundException, IOException {
        
        Encrypt en = new Encrypt();
        en.initKeyList();
        
        FileReader fr = new FileReader(thisFile);
        BufferedReader br = new BufferedReader(fr);
        
        FileWriter fw = new FileWriter(thatFile);
        BufferedWriter bw = new BufferedWriter(fw);
            
        String s;
        StringBuilder encrypted = new StringBuilder();
        while ( (s = br.readLine()) != null) {
            encrypted.setLength(s.length());
            for (int i = 0; i < encrypted.length(); i++) {
                encrypted.setCharAt(i, en.decryptChar(s.charAt(i)));
            }
            
            //write to encrypted file.
            bw.write(encrypted.toString());
            bw.newLine();
        }
        bw.close();
    }
    
    private char encryptChar(char old) {   
        for (int i = 0; i < keyListActual.size(); i++) {
            if (keyListActual.get(i).equals(old)) {
                char c = keyList.get(i);
                return c;
            }
        }
        return old;
    }
    
    private char decryptChar(char old) {
        for (int i = 0; i < keyListActual.size(); i++) {
            if (keyList.get(i).equals(old)) {
                char c = keyListActual.get(i);
                return c;
            }
        }
        return old;
    }
    
    private void initKeyList() {
        keyList = new LinkedList();
        keyListActual = new LinkedList();
        for (int i = 32; i <= 126; i++) {
            char c = (char)i;
            keyList.add(c);
            keyListActual.add(c);
        }
        //give it a seed based on some checksum
        Collections.shuffle(keyList, new Random(checksum));
    }
    
    
}
