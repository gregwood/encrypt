
package encrypt;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author weirdvector
 */
public class TestEncrypt extends Application {
    
    Encrypt en;
    TextArea thisText;
    TextArea thatText;
    PasswordField passField;
    Button btnEncrypt;
    Button btnDecrypt;
    
    public void start(Stage primaryStage) {
        en = new Encrypt();
        thisText = new TextArea();
        thisText.setWrapText(true);
        thatText = new TextArea();
        thatText.setWrapText(true);
        Label lblPass = new Label("Passphrase:");
        passField = new PasswordField();
        btnEncrypt = new Button("Encrypt");
        btnEncrypt.setOnAction(e -> encrypt_click());
        btnDecrypt = new Button("Decrypt");
        btnDecrypt.setOnAction(e -> decrypt_click());
        
        HBox controls = new HBox();
        controls.getChildren().addAll(lblPass, passField, btnEncrypt, btnDecrypt);
        VBox pane = new VBox();
        pane.getChildren().addAll(thisText, controls, thatText);
        
        Scene scene = new Scene(pane, 400, 380);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Encrypt/Decrypt");
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    
    public static void main(String[] args) {

        launch(args);
    }
    
    public void encrypt_click() {
        thatText.setText(en.en(thisText.getText(), passField.getText()));
    }
    
    public void decrypt_click() {
        thatText.setText(en.de(thisText.getText(), passField.getText()));
    }
}
